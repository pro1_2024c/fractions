package fractions;

public class NumericUtils
{
    public static long gcd(long a, long b)
    {
        if(a <= 0 || b <= 0 )
        {
            throw new IllegalArgumentException("Parameters must be positive");
        }

        long u = a;
        long w = b;
        while (w > 0)
        {
            long temp = u % w;
            u = w;
            w = temp;
        }
        return u;
    }
}
