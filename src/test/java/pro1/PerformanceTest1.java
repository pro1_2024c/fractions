package pro1;

import fractions.Fraction;

import java.util.Random;

class PerformanceTest1
{
    @org.junit.jupiter.api.Test
    void test()
    {
        int count = 5_000_000;
        Fraction[] pole = new Fraction[count];
        Random random = new Random();
        long start = System.currentTimeMillis();
        for (int i = 0; i < count; i++)
        {
            Fraction f = new Fraction(
                    random.nextLong(),
                    random.nextLong());
            pole[i] = f;
        }
        long end = System.currentTimeMillis();
        long duration = end - start;
        double fractionsPerSecond =  1000d * count / duration;
        System.out.println(fractionsPerSecond);
    }
}